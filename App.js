import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SearchScreen from './src/screens/SearchScreen';
import RestaurantShowScreen from './src/screens/RestaurantShowScreen';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Search"
        // screenOptions={{ gestureEnabled: false }}
      >
        <Stack.Screen
          name="Search"
          component={SearchScreen}
          options={{ title: 'Business Search' }}
        />
        <Stack.Screen 
          name="RestaurantShow"
          component={RestaurantShowScreen}
          options={{ title: 'Restaurant'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
// const navigator = createStackNavigator(
//   {
//     Search:  SearchScreen
//   },
//   {
//     initialRouteName: 'Search',
//     defaultNavigationOptions: {
//       title: 'Business Search',
//     }
//   }
// );

// export default createAppContainer(navigator);
