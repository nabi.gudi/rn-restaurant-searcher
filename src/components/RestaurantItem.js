import React, { useState } from 'react';
import { View, Text, StyleSheet, Image} from 'react-native';

const RestaurantItem = ({ restaurant }) => {

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: restaurant.image_url}}/>
      <Text style={styles.name}>{restaurant.name}</Text>
      <Text style={styles.subtitle}>{restaurant.rating} Stars, {restaurant.review_count} Reviews</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    marginLeft: 15,
  },
  name: {
    fontWeight: 'bold'
  },
  subtitle: {
    color: '#888'
  },
  image: {
    width: 250,
    height: 120,
    borderRadius: 4
  }
});

export default RestaurantItem;