import React, { useState } from 'react';
import { View, Text, StyleSheet, FlatList} from 'react-native';
import RestaurantItem from './RestaurantItem'
import { TouchableOpacity } from 'react-native-gesture-handler';
import  { withNavigation } from '@react-navigation/compat';

const RestaurantsList = ( { title, restaurants, navigation } ) => {
  if (!restaurants.length) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <FlatList 
        horizontal
        data={restaurants}
        keyExtractor={(restaurant) => restaurant.id}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity 
              onPress={() => navigation.navigate('RestaurantShow', {id: item.id})}
            >
              <RestaurantItem restaurant={item} />
            </TouchableOpacity>
          );
        }}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: '500',
    marginBottom: 5,
    marginLeft: 15
  },
  results: {
    marginTop: 10,
    marginBottom: 5,
    marginHorizontal: 15
  },
  container: {
    marginBottom: 15
  }
});

export default withNavigation(RestaurantsList);