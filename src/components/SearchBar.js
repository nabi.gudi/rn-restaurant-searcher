import React from 'react';
import { View, TextInput, StyleSheet} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const SearchBar = ({ term, onTermChange, onTermSubmit }) => {
  return (
    <View style={styles.background}>
        <FontAwesome 
          name='search' 
          style={styles.icon}
        />
        <TextInput 
          style={styles.text} 
          placeholder='Search'
          autoCapitalize='none'
          autoCorrect={false}
          value={term}
          onChangeText={onTermChange}
          onEndEditing={onTermSubmit}
        />
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 15,
    flex: 1,
    fontSize: 18
  },
  background: {
    backgroundColor: '#F0EEEE',
    height: 50,
    borderRadius: 5,
    margin: 15,
    flexDirection: 'row',
  },
  icon: {
    marginRight: 10,
    fontSize: 20,
    alignSelf: 'center',
    marginHorizontal: 10,
  }
});

export default SearchBar;