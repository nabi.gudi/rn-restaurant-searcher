import { useEffect, useState } from 'react';
import yelp from '../api/yelp';

export const useResults = () => {
  const [restaurants, setRestaurants] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  const searchAPI = async (searchTerm) => {
    try {
      const response = await yelp.get('/search', {
        params: {
          limit: 50,
          term: searchTerm,
          location: 'san diego'
        }
      });
      setRestaurants(response.data.businesses);
    } catch (err) {
      setErrorMessage('Something went wrong :(');
    }
  }

  useEffect(() => {
    searchAPI('pasta');
  }, []);

  return [searchAPI, restaurants, errorMessage];
};