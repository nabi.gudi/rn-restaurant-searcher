import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList} from 'react-native';
import yelp from '../api/yelp'

const RestaurantShowScreen = ( { route, navigation }) => {
  const [restaurant, setRestaurant] = useState(null);
  const { id } = route.params;

  const getRestaurant = async (id) => {
    const response = await yelp.get(`/${id}`);
    return setRestaurant(response.data);
  };

  useEffect(() => {
    getRestaurant(id)
  }, []);

  if (!restaurant) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.name}>{restaurant.name}</Text>
      <Text style={styles.text}>There're some pics of the restaurant you choose: </Text>
      <FlatList 
        data={restaurant.photos}
        keyExtractor={photo => photo}
        renderItem={ ({item}) => {
          return <Image style={styles.image} source={{ uri: item }} />
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    marginLeft: 15,
  },
  name: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  text: {
    fontSize: 15,
    marginVertical: 5
  },
  subtitle: {
    color: '#888'
  },
  image: {
    width: 300,
    height: 200,
    borderRadius: 4, 
    marginVertical: 10
  }
});

export default RestaurantShowScreen;