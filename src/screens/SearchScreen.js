import React, { useState } from 'react';
import { View, Text, StyleSheet} from 'react-native';
import SearchBar from '../components/SearchBar';
import { useResults } from '../hooks/useResults';
import RestaurantsList from '../components/RestaurantsList';
import { ScrollView } from 'react-native-gesture-handler';

const SearchScreen = () => {
  const [term, setTerm] = useState('');
  const [searchAPI, restaurants, errorMessage] = useResults();
 
  const filterResultByPrice = (price) => {
    return restaurants.filter((result) => {
      return result.price === price;
    })
  }

  return (
    <View style={styles.background}>
      <SearchBar 
        term={term} 
        onTermChange={setTerm} 
        onTermSubmit={() => searchAPI(term)}
        />
      {!errorMessage ? null : 
        <Text style={styles.error}>{errorMessage}</Text>
      }
      <Text style={styles.text}>We found {restaurants.length} restaurants</Text>
      <ScrollView>
        <RestaurantsList 
          title={'Cost Effective'} 
          restaurants={filterResultByPrice('$')} 
        />
        <RestaurantsList 
          title={'Bit Pricier'} 
          restaurants={filterResultByPrice('$$')} 
        />
        <RestaurantsList 
          title={'Big Splender'} 
          restaurants={filterResultByPrice('$$$')} 
        />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 15,
    marginHorizontal: 15,
    marginBottom: 10
  },
  error: {
    fontSize: 13,
    color: 'red'
  },
  background: {
    backgroundColor: '#FFF',
    flex: 1,
  }
});

export default SearchScreen;